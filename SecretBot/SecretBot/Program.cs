﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using SecretBot.Models;
using SecretBot.Models.FinalQuest;
using SecretBot.Services;
using SecretBot.SettingsAndKeys;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using File = System.IO.File;

namespace SecretBot
{
    public static class Program
    {
        private static readonly TelegramBotClient Bot =
            new TelegramBotClient("791741627:AAHXLQ3menqg9jC465M9PfKbY3-6wT0hjGQ");

        private static readonly ConversationService conversationService = new ConversationService();
        private static readonly FinalQuestService finalQuestService = new FinalQuestService();

        public static void Main(string[] args)
        {
            var me = Bot.GetMeAsync().Result;
            Console.Title = me.Username;

            Bot.OnMessage += BotOnMessageReceived;
            Bot.OnMessageEdited += BotOnMessageReceived;
            Bot.OnCallbackQuery += BotOnCallbackQueryReceived;
            Bot.OnReceiveError += BotOnReceiveError;

            Bot.StartReceiving(Array.Empty<UpdateType>());
            Console.WriteLine($"Start listening for @{me.Username}");
            Console.ReadLine();
            Bot.StopReceiving();
        }

        private static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message;

            if (message == null)
            {
                return;
            }

            if (message.Type == MessageType.Text && message.Text == "/start")
            {
                await ProcessStart(message.Chat.Id).ConfigureAwait(false);
                return;
            }

            if (message.Type == MessageType.Text && message.Text == "/casino")
            {
                await ProcessCasino(message.Chat.Id).ConfigureAwait(false);
                return;
            }

            if (message.Type == MessageType.Text && message.Text.Contains("newlevel"))
            {
                await ProcessNewLevel(message).ConfigureAwait(false);
                return;
            }

            var fileWasSaved = false;
            if (message.Type == MessageType.Photo)
            {
                var file = await Bot.GetFileAsync(message.Photo.LastOrDefault()?.FileId);

                var filename = message.Chat.Id + "." + file.FilePath.Split('.').Last();

                using (var saveImageStream = File.Open(filename, FileMode.Create))
                {
                    await Bot.DownloadFileAsync(file.FilePath, saveImageStream);
                }

                fileWasSaved = true;
                await SendPhotoAsync(message, filename, "Команда прислала фотку").ConfigureAwait(false);
            }

            var userId = messageEventArgs.Message.From.Id;
            var userName = messageEventArgs.Message.From.Username
                           ?? $"{messageEventArgs.Message.From.FirstName} {messageEventArgs.Message.From.LastName}";

            var result =
                conversationService.StartConversation(userId, userName, message.Chat.Id, message, fileWasSaved);

            if (result is KeyboardResult keyboardResult)
            {
                await SendInlineKeyboardMarkup(message, keyboardResult.Message, keyboardResult.Ids)
                    .ConfigureAwait(false);
            }
            else
            {
                await SendMessageToAdmins(result).ConfigureAwait(false);
                await Bot.SendTextMessageAsync(
                    message.Chat.Id,
                    result.Message).ConfigureAwait(false);
            }
        }

        private static async Task ProcessNewLevel(Message message)
        {
            if (!AdminKeys.AdminChatIds.Contains(message.Chat.Id))
            {
                return;
            }

            var parts = message.Text.Split(' ');
            if (parts.Length < 2)
            {
                return;
            }

            var teamId = parts[1];
            var result = conversationService.MoveLevelUp(teamId, message);
            if (!result.IsSuccess)
            {
                await Bot.SendTextMessageAsync(
                    message.Chat.Id,
                    result.Message).ConfigureAwait(false);
            }

            var chatsWithTeam = conversationService.SelectChatsWithTeam(teamId);
            foreach (var chatId in chatsWithTeam)
            {
                await Bot.SendTextMessageAsync(
                    chatId,
                    result.Message).ConfigureAwait(false);
            }
        }

        private static async Task ProcessStart(long chatId)
        {
            await Bot.SendTextMessageAsync(
                chatId,
                "Говорят, кто не рискует, тот не пьет шампанского. Сегодня мы выясним, кто из вас самый рисковый, азартный и смелый. Сегодня мы узнаем, какая команда обыграет казино и сорвёт джек-пот!"
                + Environment.NewLine
                + "Делайте ваши ставки в нашем"
                + Environment.NewLine
                + "/casino,"
                + Environment.NewLine
                + "господа! Игра начинается.").ConfigureAwait(false);
        }

        private static async Task ProcessCasino(long chatId)
        {
            await Bot.SendTextMessageAsync(
                chatId,
                "Разгадайте загадку биоробота и введите код, который вам выдал биоробот.").ConfigureAwait(false);
        }

        private static async Task SendMessageToAdmins(Result result)
        {
            if (result.IsSuccess)
            {
                foreach (var adminChatId in AdminKeys.AdminChatIds)
                {
                    await Bot.SendTextMessageAsync(
                        adminChatId,
                        $"Команда {result.TeamName} прошла этап {result.Level}").ConfigureAwait(false);
                }
            }
        }

        private static Task SendInlineKeyboardMarkup(Message message, string text, string[] ids)
        {
            var buttonses = ids.Select((x, i) =>
                InlineKeyboardButton.WithCallbackData((i + 1).ToString(), x));

            var inlineKeyboard = new InlineKeyboardMarkup(buttonses);
            return Bot.SendTextMessageAsync(
                message.Chat.Id,
                text,
                replyMarkup: inlineKeyboard);
        }

        private static async Task SendPhotoAsync(Message message, string file, string caption)
        {
            foreach (var adminChatId in AdminKeys.AdminChatIds)
            {
                using (var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    await Bot.SendPhotoAsync(
                            adminChatId,
                            fileStream,
                            caption)
                        .ConfigureAwait(false);
                }
            }
        }

        private static async void BotOnCallbackQueryReceived(object sender,
            CallbackQueryEventArgs callbackQueryEventArgs)
        {
            var chosenInlineResult = callbackQueryEventArgs.CallbackQuery;

            var userId = chosenInlineResult.From.Id;
            var userName = chosenInlineResult.From.Username
                           ?? $"{chosenInlineResult.From.FirstName} {chosenInlineResult.From.LastName}";

            var result = finalQuestService.StartConversation(userId, userName, chosenInlineResult);
            if (result != null && result is KeyboardResult keyboardResult)
            {
                if (keyboardResult.Ids == null || keyboardResult.Ids.Length == 0)
                {
                    keyboardResult.Level = Level.Final;
                    await SendMessageToAdmins(keyboardResult);
                }

                await SendInlineKeyboardMarkup(callbackQueryEventArgs.CallbackQuery.Message, keyboardResult.Message,
                        keyboardResult.Ids)
                    .ConfigureAwait(false);
            }
        }

        private static void BotOnReceiveError(object sender, ReceiveErrorEventArgs receiveErrorEventArgs)
        {
            Console.WriteLine("Received error: {0} — {1}",
                receiveErrorEventArgs.ApiRequestException.ErrorCode,
                receiveErrorEventArgs.ApiRequestException.Message);
        }
    }
}
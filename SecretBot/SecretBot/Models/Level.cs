﻿namespace SecretBot.Models
{
    public enum Level
    {
        Storage = 1,
        FindScissors = 2,
        Scissors = 3,
        SwitchOffCamera = 4,
        MotionSensors = 5,
        Photos = 6,
        SafeHacking = 7,
        Final = 8
    }
}
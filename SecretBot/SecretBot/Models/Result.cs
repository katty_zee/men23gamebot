﻿namespace SecretBot.Models
{
    public class Result
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string TeamName { get; set; }
        public Level Level { get; set; }

        public static Result CreateFailed(string message = null)
        {
            return new Result {IsSuccess = false, Message = message ?? "Код неверный. Попробуйте ещё раз." };
        }

        public static Result CreateSuccess(string message, string teamName, Level level)
        {
            return new Result { IsSuccess = true, Message = message, Level = level, TeamName = teamName};
        }
    }
}
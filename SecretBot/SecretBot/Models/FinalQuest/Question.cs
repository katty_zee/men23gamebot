﻿namespace SecretBot.Models.FinalQuest
{
    public class Question
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public Answer[] Answers { get; set; }
    }
}
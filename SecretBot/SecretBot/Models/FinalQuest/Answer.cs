﻿namespace SecretBot.Models.FinalQuest
{
    public class Answer
    {
        public string Id { get; set; }
        public string NextQuestionId { get; set; }
    }
}
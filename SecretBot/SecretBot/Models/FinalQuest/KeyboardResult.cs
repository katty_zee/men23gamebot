﻿namespace SecretBot.Models.FinalQuest
{
    public class KeyboardResult : Result
    {
        public string[] Ids { get; set; }

        public static KeyboardResult CreateSuccess(string message, string[] ids, string userName)
        {
            return new KeyboardResult { IsSuccess = true, Message = message, Ids = ids, TeamName = userName};
        }
    }
}
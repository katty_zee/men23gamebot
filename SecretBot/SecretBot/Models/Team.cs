﻿namespace SecretBot.Models
{
    public class Team
    {
        public string Id { get; set; }
        public Level Level { get; set; }
        public string GirlName { get; set; }
    }
}
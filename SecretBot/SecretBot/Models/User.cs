﻿namespace SecretBot.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Team { get; set; }
        public string Name { get; set; }
        public string CurrentQuestionId { get; set; }
        public long ChatId { get; set; }
    }
}
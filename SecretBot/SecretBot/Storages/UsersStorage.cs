﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using SecretBot.Models;

namespace SecretBot.Storages
{
    public class UsersStorage
    {
        private static ConcurrentDictionary<int,User> users = new ConcurrentDictionary<int, User>();

        public User Find(int id)
        {
            users.TryGetValue(id, out var user);

            var userFilePath = GetUserFilePath(id);
            if (user == null && File.Exists(userFilePath))
            {
                var userText = File.ReadAllText(userFilePath);
                user = JsonConvert.DeserializeObject<User>(userText);
                users.AddOrUpdate(id, user, (key, x) => x);
            }
            return user;
        }

        public User[] SelectByTeam(string teamId)
        {
            return users.Values.Where(x => x.Team == teamId).ToArray();
        }

        public void CreateUser(int id, string team, string name, long userChatId)
        {
            if (users.ContainsKey(id))
            {
                Console.WriteLine($"User {id} already exists");
                return;
            }

            var user = new User
            {
                Id = id,
                Team = team,
                Name = name,
                CurrentQuestionId = "1",
                ChatId = userChatId
            };
            users.AddOrUpdate(id, user, (key, x) => x);

            var userPath = GetUserFilePath(id);
            if (!File.Exists(userPath))
            {
                var userText = JsonConvert.SerializeObject(user);
                File.WriteAllText(userPath, userText);
            }
        }

        public void SaveCurrentQuestion(int id, string questionId)
        {
            users[id].CurrentQuestionId = questionId;

            var teamFilePath = GetUserFilePath(id);
            if (File.Exists(teamFilePath))
            {
                var user = users[id];
                var text = JsonConvert.SerializeObject(user);
                File.WriteAllText(teamFilePath, text);
            }
        }
        private string GetUserFilePath(int id) => $"{id}";
    }
}
﻿using System.Collections.Concurrent;
using System.IO;
using Newtonsoft.Json;
using SecretBot.Models;
using SecretBot.SettingsAndKeys;

namespace SecretBot.Storages
{
    public class TeamStorage
    {
        private static ConcurrentDictionary<string, Team> teams = new ConcurrentDictionary<string, Team>();

        public Team Find(string id)
        {
            teams.TryGetValue(id, out var team);

            var teamFilePath = GetTeamFilePath(id);
            if (team == null && File.Exists(teamFilePath))
            {
                var teamText = File.ReadAllText(teamFilePath);
                team = JsonConvert.DeserializeObject<Team>(teamText);
                teams.AddOrUpdate(id, team, (key, x) => x);
            }

            return team;
        }

        public void Create(string id)
        {
            if (teams.ContainsKey(id))
            {
                return;
            }

            var team = new Team
            {
                Id = id,
                Level = Level.Storage,
                GirlName = Teams.Names[id]
            };
            teams.AddOrUpdate(id, team, (key, x) => x);

            var teamFilePath = GetTeamFilePath(id);
            if (!File.Exists(teamFilePath))
            {
                var teamText = JsonConvert.SerializeObject(team);
                File.WriteAllText(teamFilePath, teamText);
            }
        }

        public void UpdateTeamLevel(string id, Level level)
        {
            teams[id].Level = level;

            var teamFilePath = GetTeamFilePath(id);
            if (File.Exists(teamFilePath))
            {
                var team = teams[id];
                var teamText = JsonConvert.SerializeObject(team);
                File.WriteAllText(teamFilePath, teamText);
            }
        }

        private string GetTeamFilePath(string id) => id.Replace('/', '_');

    }
}
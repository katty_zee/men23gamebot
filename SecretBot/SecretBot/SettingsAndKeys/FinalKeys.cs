﻿using System.Collections.Generic;

namespace SecretBot.SettingsAndKeys
{
    public static class FinalKeys
    {
        public static Dictionary<string, string> Map = new Dictionary<string, string>()
        {
            {Teams.Roulette, "8983"},
            {Teams.Baccarat, "8091"},
            {Teams.Blackjack, "8293"},
            {Teams.Dice, "8277"},
            {Teams.Poker, "8172"},
            {Teams.TexasHoldem, "8992"},
            {Teams.OneArmedBandit, "8123"},
            {Teams.Solitaire, "8176"},
            {Teams.Kerchief, "8394"},
            {Teams.Scorpius, "8982"},
            {Teams.Bridge, "8765"}
        };
    }
}
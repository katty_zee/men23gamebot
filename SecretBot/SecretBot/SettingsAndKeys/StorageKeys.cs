﻿using System.Collections.Generic;

namespace SecretBot.SettingsAndKeys
{
    //2 - код для старта задания с дверью и красными лазерами (хранилища)
    public static class StorageKeys
    {
        public static Dictionary<string, string> Map = new Dictionary<string, string>()  
        {
            {Teams.Roulette, "3958"},
            {Teams.Baccarat, "1654"},
            {Teams.Blackjack, "9375"},
            {Teams.Dice, "2837"},
            {Teams.Poker, "4776"},
            {Teams.TexasHoldem, "2009"},
            {Teams.OneArmedBandit, "2765"},
            {Teams.Solitaire, "6825"},
            {Teams.Kerchief, "9873"},
            {Teams.Scorpius, "1234"},
            {Teams.Bridge, "9847"},
        };
    }
    
    //3 - код для нахождения ножниц
    public static class ScissorsFindKeys
    {
        public static Dictionary<string, string> Map = new Dictionary<string, string>()
        {
            {Teams.Roulette, "7639"},
            {Teams.Baccarat, "7862"},
            {Teams.Blackjack, "7371"},
            {Teams.Dice, "7865"},
            {Teams.Poker, "7254"},
            {Teams.TexasHoldem, "7265"},
            {Teams.OneArmedBandit, "7098"},
            {Teams.Solitaire, "7980"},
            {Teams.Kerchief, "7653"},
            {Teams.Scorpius, "7261"},
            {Teams.Bridge, "7239"}
        };
    }

    //4 - код для загадки с камерой
    public static class ScissorsKeys
    {
        public static Dictionary<string, string> Map = new Dictionary<string, string>()
        {
            {Teams.Roulette, "1392"},
            {Teams.Baccarat, "9836"},
            {Teams.Blackjack, "4667"},
            {Teams.Dice, "3772"},
            {Teams.Poker, "8727"},
            {Teams.TexasHoldem, "1909"},
            {Teams.OneArmedBandit, "2081"},
            {Teams.Solitaire, "3927"},
            {Teams.Kerchief, "9683"},
            {Teams.Scorpius, "2742"},
            {Teams.Bridge, "8170"}
        };
    }
    // 5 - код для отключения камер
    public static class SwitchOffCameraKeys
    {
        public static Dictionary<string, string> Map = new Dictionary<string, string>()
        {
            {Teams.Roulette, "3662"},
            {Teams.Baccarat, "0918"},
            {Teams.Blackjack, "0284"},
            {Teams.Dice, "0283"},
            {Teams.Poker, "0285"},
            {Teams.TexasHoldem, "0374"},
            {Teams.OneArmedBandit, "0787"},
            {Teams.Solitaire, "0267"},
            {Teams.Kerchief, "0298"},
            {Teams.Scorpius, "0394"},
            {Teams.Bridge, "0965"}
        };
    }
    //6 - код для запуска ФБР с фотками
    public static class MotionSensorsKeys
    {
        public static Dictionary<string, string> Map = new Dictionary<string, string>()
        {
            {Teams.Roulette, "4568"},
            {Teams.Baccarat, "4932"},
            {Teams.Blackjack, "4615"},
            {Teams.Dice, "4098"},
            {Teams.Poker, "4716"},
            {Teams.TexasHoldem, "4827"},
            {Teams.OneArmedBandit, "4032"},
            {Teams.Solitaire, "4928"},
            {Teams.Kerchief, "4551"},
            {Teams.Scorpius, "4982"},
            {Teams.Bridge, "4991"}
        };
    }
    
    //7 - код для начала взлома сейфа
    public static class PhotosKeys
    {
        public static Dictionary<string, string> Map = new Dictionary<string, string>()
        {
            {Teams.Roulette, "5281"},
            {Teams.Baccarat, "5281"},
            {Teams.Blackjack, "5281"},
            {Teams.Dice, "5281"},
            {Teams.Poker, "5281"},
            {Teams.TexasHoldem, "5281"},
            {Teams.OneArmedBandit, "5281"},
            {Teams.Solitaire, "5281"},
            {Teams.Kerchief, "5281"},
            {Teams.Scorpius, "5281"},
            {Teams.Bridge, "5281"}
        };
    }
    
    //8 - код для взлома сейфа
    public static class SafeHackingKeys
    {
        public static Dictionary<string, string> Map = new Dictionary<string, string>()
        {
            {Teams.Roulette, "6995"},
            {Teams.Baccarat, "6985"},
            {Teams.Blackjack, "6127"},
            {Teams.Dice, "6918"},
            {Teams.Poker, "6728"},
            {Teams.TexasHoldem, "6574"},
            {Teams.OneArmedBandit, "6090"},
            {Teams.Solitaire, "6871"},
            {Teams.Kerchief, "6009"},
            {Teams.Scorpius, "9384"},
            {Teams.Bridge, "9934"}
        };
    }
    //9 - код для отхода (финальный квест)
    public static class RetreatKeys
    {
        public static Dictionary<string, string> Map = new Dictionary<string, string>()
        {
            {Teams.Roulette, "8983"},
            {Teams.Baccarat, "8091"},
            {Teams.Blackjack, "8293"},
            {Teams.Dice, "8277"},
            {Teams.Poker, "8172"},
            {Teams.TexasHoldem, "8992"},
            {Teams.OneArmedBandit, "8123"},
            {Teams.Solitaire, "8176"},
            {Teams.Kerchief, "8394"},
            {Teams.Scorpius, "8982"},
            {Teams.Bridge, "8765"}
        };
    }
}
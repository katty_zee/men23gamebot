﻿using System.Collections.Generic;

namespace SecretBot.SettingsAndKeys
{
    public static class Teams
    {
        public const string Roulette = "Рулетка/Roulette";
        public const string Baccarat = "Баккара/Baccarat";
        public const string Blackjack = "Блэкджек/Blackjack";
        public const string Dice = "Кости/Dice";
        public const string Poker = "Покер/Poker";
        public const string TexasHoldem = "Техасский холдем/Texas hold'em";
        public const string OneArmedBandit = "Однорукий бандит/One-armed bandit";
        public const string Solitaire = "Пасьянс/Solitaire";
        public const string Kerchief = "Косынка/Kerchief";
        public const string Scorpius = "Скорпион/Scorpius";
        public const string Bridge = "Бридж/Bridge";

        //1 - код для задания с картами
        public static Dictionary<string, string> Map = new Dictionary<string, string>()
        {
            {"1746", Roulette },
            {"1998", Baccarat },
            {"4645", Blackjack },
            {"9856", Dice },
            {"8746", Poker },
            {"2653", TexasHoldem },
            {"5672", OneArmedBandit },
            {"9879", Solitaire },
            {"4875", Kerchief },
            {"1204", Scorpius },
            {"1782", Bridge },
        };

        public static Dictionary<string, string> Names = new Dictionary<string, string>()
        {
            {Teams.Roulette, "Веру Тимохину"},
            {Teams.Baccarat, "Настю Митрофанову"},
            {Teams.Blackjack, "Катю Климову"},
            {Teams.Dice, "Аню Лебедеву"},
            {Teams.Poker, "Вику Шмитову"},
            {Teams.TexasHoldem, "Регину Кашину"},
            {Teams.OneArmedBandit, "Алену Соколову"},
            {Teams.Solitaire, "Машу Новгородову"},
            {Teams.Kerchief, "Юлю Тарасенко"},
            {Teams.Scorpius, "Настю Дерендееву"},
            {Teams.Bridge, "Оксану Запорожец"}
        };
    }
}
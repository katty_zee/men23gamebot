﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SecretBot.Models;
using SecretBot.Models.FinalQuest;
using SecretBot.SettingsAndKeys;
using SecretBot.Storages;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using File = System.IO.File;
using User = SecretBot.Models.User;

namespace SecretBot.Services
{
    public class ConversationService
    {
        private readonly UsersStorage usersStorage;
        private readonly TeamStorage teamStorage;
        private readonly string successMessage = "Код верный. Следующее задание: \n";

        public ConversationService()
        {
            usersStorage = new UsersStorage();
            teamStorage = new TeamStorage();
        }

        public Result StartConversation(int userId, string userName, long userChatId, Message message, bool fileWasSaved)
        {
            var user = usersStorage.Find(userId);

            if (user == null)
            {
                return ProcessFirstMessage(userId, userName, message, userChatId);
            }

            var team = teamStorage.Find(user.Team);
            if (team == null)
            {
                teamStorage.Create(user.Team);
                team = teamStorage.Find(user.Team);
            }

            return ProcessCode(user, team, message, fileWasSaved);
        }

        /// <summary>
        /// Этот метод для админа, чтобы он мог поднять уровень команде
        /// Если команды нет, чувак получит ошибку
        /// </summary>
        public Result MoveLevelUp(string teamId, Message message)
        {
            var team = teamStorage.Find(teamId);
            if ( team == null )
            {
                return Result.CreateFailed($"Команды {teamId} не существует.");
            }

            var nextLevel = GetNextLevel(team.Level);
            teamStorage.UpdateTeamLevel(team.Id, nextLevel);
            var textForNextLevel = GetTextForLevel(nextLevel);
            // тут левел не нужен, просто так передаем
            return Result.CreateSuccess(successMessage + textForNextLevel, team.Id, Level.Storage);
        }

        /// <summary>
        /// Этот метод обрабатывает ситуацию когда пользователь в первый раз обратился за обработкой кода.
        /// Тогда мы считаем что это первый код, по которому надо определить его команду
        /// После того как определили команду отдаем текст для текущего уровня команды
        /// </summary>
        private Result ProcessFirstMessage(int userId, string userName, Message message, long userChatId)
        {
            if (message.Type != MessageType.Text)
            {
                Result.CreateFailed();
            }
            var messageText = message.Text;
            if (Teams.Map.TryGetValue(messageText, out var teamId))
            {
                usersStorage.CreateUser(userId, teamId, userName, userChatId);

                var team = teamStorage.Find(teamId);
                if (team == null)
                {
                    teamStorage.Create(teamId);
                    team = teamStorage.Find(teamId);
                }

                var textForLevel = GetTextForLevel(team.Level);
                return Result.CreateSuccess(successMessage + textForLevel, teamId, team.Level);
            }

            return Result.CreateFailed();
        }

        /// <summary>
        /// Этот метод для обработки ответа команды и установки следующего уровня если все ок
        /// Или ответа об ошибке если нет
        /// </summary>
        private Result ProcessCode(User user, Team team, Message message, bool fileWasSaved)
        {
            if (message.Type != MessageType.Text && team.Level != Level.Photos)
            {
                Result.CreateFailed();
            }
            switch (team.Level)
            {
                case Level.Storage:
                    return ProcessStorageLevel(user, team, message);
                case Level.FindScissors:
                    return ProcessFindScissorsLevel(user, team, message);
                case Level.Scissors:
                    return ProcessScissorsLevel(user, team, message);
                case Level.SwitchOffCamera:
                    return SwitchOffCamera(user, team, message);
                case Level.MotionSensors:
                    return ProcessMotionSensorsLevel(user, team, message);
                case Level.Photos:
                    return ProcessPhotosLevel(user, team, message, fileWasSaved);
                case Level.SafeHacking:
                    return ProcessSafeHackingLevel(user, team, message);
                case Level.Final:
                    var messageText = message.Text;
                    if (FinalKeys.Map.TryGetValue(user.Team, out var storageKey))
                    {
                        if (storageKey == messageText)
                        {
                            var question = QuestionStorage.questions.First();
                            return KeyboardResult.CreateSuccess(question.Text, question.Answers.Select(x => x.Id).ToArray(), user.Name);
                        }

                        return Result.CreateFailed("Тебе нужно пройти квест до конца.");
                    }

                    return Result.CreateFailed(
                        $"{user.Name}, для твоей команды {team.Id} уровень {team.Level} не настроен. Обратись к админам Кате З. и Аннушке.");
                default:
                    return Result.CreateFailed(
                        $"{user.Name}, для твоей команды {team.Id} уровень {team.Level} не настроен. Обратись к админам Кате З. и Аннушке.");
            }
        }

        public long[] SelectChatsWithTeam(string teamId)
        {
            var teams = usersStorage.SelectByTeam(teamId);
            return teams.Select(x => x.ChatId).ToArray();
        }
        private Result ProcessTextAnswer(
            User user,
            Team team,
            Message message,
            Dictionary<string, string> map,
            Level level)
        {
            
            var messageText = message.Text;
            if (map.TryGetValue(user.Team, out var storageKey))
            {
                if (storageKey == messageText)
                {
                    var nextLevel = GetNextLevel(level);
                    teamStorage.UpdateTeamLevel(team.Id, nextLevel);
                    var textForNextLevel = String.Empty;
                    if (nextLevel == Level.Final)
                    {
                        textForNextLevel = GetTextForLevel(nextLevel, team.GirlName);
                    }
                    else
                    {
                        textForNextLevel = GetTextForLevel(nextLevel);
                    }
                    return Result.CreateSuccess(successMessage + textForNextLevel, team.Id, level);
                }

                return Result.CreateFailed();
            }

            return Result.CreateFailed(
                $"{user.Name}, для твоей команды {team.Id} уровень {team.Level} не настроен. Обратись к админам Кате З. и Аннушке.");
        }

        private Result ProcessStorageLevel(User user, Team team, Message message)
        {
            return ProcessTextAnswer(user, team, message, StorageKeys.Map, Level.Storage);
        }

        private Result ProcessFindScissorsLevel(User user, Team team, Message message)
        {
            return ProcessTextAnswer(user, team, message, ScissorsFindKeys.Map, Level.FindScissors);
        }

        private Result ProcessScissorsLevel(User user, Team team, Message message)
        {
            return ProcessTextAnswer(user, team, message, ScissorsKeys.Map, Level.Scissors);
        }

        private Result SwitchOffCamera(User user, Team team, Message message)
        {
            return ProcessTextAnswer(user, team, message, SwitchOffCameraKeys.Map, Level.SwitchOffCamera);
        }

        private Result ProcessMotionSensorsLevel(User user, Team team, Message message)
        {
            return ProcessTextAnswer(user, team, message, MotionSensorsKeys.Map, Level.MotionSensors);
        }

        private Result ProcessPhotosLevel(User user, Team team, Message message, bool fileWasSaved)
        {
            if (message.Type == MessageType.Photo)
            {
                if (fileWasSaved)
                {
                    return Result.CreateSuccess("Ммм, какое фото! А теперь давай код!", team.Id, Level.Photos);
                }
                return Result.CreateFailed("Что-то пошло не так");
            }

            if (File.Exists($"{message.Chat.Id}.jpg")|| File.Exists($"{message.Chat.Id}.png") || File.Exists($"{message.Chat.Id}.jpeg"))
            {
                return ProcessTextAnswer(user, team, message, PhotosKeys.Map, Level.Photos);
            }
            return Result.CreateFailed("ФБР скоро нажмет кнопку! Присылай скорее фото! А код потом.");
        }

        private Result ProcessSafeHackingLevel(User user, Team team, Message message)
        {
            return ProcessTextAnswer(user, team, message, SafeHackingKeys.Map, Level.SafeHacking);
        }


        private Level GetNextLevel(Level l) => l + 1;

        private string GetTextForLevel(Level level, string name = null)
        {
            switch (level)
            {
                case Level.Storage:
                    return
                        "Изучите карты и найдите хранилище казино. Когда ваш взломщик доберётся до места, узнайте у него код и отправьте в бот.";
                case Level.FindScissors:
                    return
                        "Вы нашли хранилище казино. Оно защищено лазерными лучами. Чтобы попасть внутрь, нужно ввести в бот 4-значный код, получить карту, найти антилазерные ножницы и освободить вход. Чтобы составить код, используйте инструкцию по расшифровке цветов лазеров.";
                case Level.Scissors:
                    return "Возьмите карту у своей помощницы. Найдите антилазерные ножницы и введите код, который вы нашли рядом с ними.";
                case Level.SwitchOffCamera:
                    return "Поздравляем, вы проникли в хранилище! Внимание: вам нужно отключить камеры, чтобы охрана не увидела вашего взломщика. Для этого хакните алгоритм системы слежения на ноутбуке в комнате с помощью инструкции.";
                case Level.MotionSensors:
                    return
                        "Отлично! Камера отключена, но будьте осторожны с зелеными лазерами -- это датчики движения. Пробравшись мимо них бесшумно, ваш взломщик сможет добраться до кода, который отключит сигнализацию у сейфа. Если он не справится, отдуваться придётся вам, или вас всех повяжут! Если справится, пришлите в бот найденный им код.";
                case Level.Photos:
                    return
                        "Ахтунг! Алярма! Вы попали на скрытые камеры ФБР! Прямо сейчас ФБР собирается отправить фото ваших лиц охране казино. У вас есть пять минут, чтобы изменить свои фотографии в специальной программе и прислать их в бот. Ваш взломщик за это время должен добыть код, который нужно ввести, чтобы подтвердить замену фотографий.";
                case Level.SafeHacking:
                    return
                        "Отлично! Охрана не смогла вас опознать. Сигнализация и камеры отключены. Вы в шаге от главного приза. Найдите верную последовательность цифр и узнайте код от сейфа. Введите её в бот в обмен на карту к сейфу с вашим большим кушем. У вас есть 10 минут, пока не работает сигнализация и отключены камеры.";
                case Level.Final:
                    return
                        $"Поздравляем! Вы восхитительны! Казино взломано. Придётся нам ещё поработать над безопасностью казино перед открытием. Найдите {name} и возьмите у неё ключ от ячейки сейфа с вашим призом. В сейфе вы найдёте и код для получения инструкций по отходу из казино. Не теряйте времени, за вами уже выехали ФБРовцы!";
                default:
                    throw new ArgumentOutOfRangeException(nameof(level), level, null);
            }
        }
    }
}
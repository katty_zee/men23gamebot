﻿using System.Linq;
using SecretBot.Models.FinalQuest;
using SecretBot.Storages;
using Telegram.Bot.Types;

namespace SecretBot.Services
{
    public class FinalQuestService
    {
        private readonly UsersStorage usersStorage;
        public FinalQuestService()
        {
            usersStorage = new UsersStorage();
        }

        public KeyboardResult StartConversation(int userId, string userName, CallbackQuery chosenInlineResult)
        {
            var user = usersStorage.Find(userId);
            var answerId = chosenInlineResult.Data;
            var currentQuestion = QuestionStorage.GetQuestion(answerId);
            if (currentQuestion.Id != user.CurrentQuestionId)
            {
                return null;
            }

            var answer = QuestionStorage.GetAnswer(answerId);
            var question = QuestionStorage.GetNextQuestion(answer.NextQuestionId);
            usersStorage.SaveCurrentQuestion(userId, question.Id);
            return KeyboardResult.CreateSuccess(question.Text, question.Answers.Select(x => x.Id).ToArray(), user.Name);
        }
    }
}